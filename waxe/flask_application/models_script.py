# -*- coding: utf-8 -*-

from flask import current_app

from flask.ext.script import Command, Option
from flask.ext.security.confirmable import confirm_user

from flask_application.models import FlaskDocument

from flask_application.createcampaign.models import *

def convert2unicode(mydict):
    for k, v in mydict.iteritems():
        if isinstance(v, str):
            mydict[k] = unicode(v, errors = 'replace')
        elif isinstance(v, dict):
            convert2unicode(v)
            
def commit(fn):
    def wrapper(*args, **kwargs):
        fn(*args, **kwargs)
        #_datastore.commit()
    return wrapper

class PopulateParliamentariansDB(Command):
    """Fills in predefined data to DB"""
    
    option_list = (
        Option('-f', '--filename', dest='filename', default=''),
        Option('-c', '--country', dest='country', default=''),
    )
    
    def run(self, **kwargs):
        
        if kwargs["filename"] != '':
            self.create_parliamentarians_from_list(kwargs["filename"])
        elif kwargs["country"] != '':
            self.create_parliamentarians_from_list(kwargs["country"]+'_AllPartiesComplete')   
        else:            
            self.create_parliamentarians()
        
    @staticmethod
    def create_parliamentarians_from_list(filename):
        
        import csv
                
        import sys
        reload(sys)
        sys.setdefaultencoding("utf-8")
    
        csv_reader = csv.reader(open('input_docs/'+filename+'.csv','r'),delimiter=';')
        
        parsed_csv = []
    
        headers_list = []
        
        for i,row in enumerate(csv_reader):
            temp_row = []
            
            for j,element in enumerate(row): 
                
                temp_element = element
                                     
                if temp_element == ' ' or temp_element == '  ':
                    temp_element = ''
        
                temp_row.append(temp_element)
        
            parsed_csv.append(temp_row)
            
        for i,row in enumerate(parsed_csv):
            #print " ROOOOOW --> "+str(i)
            if i==0:
                headers_list = row
            else:
                
                #test_passed = True  
                #isIncluded = True
                #test_status = "False in fields: "
                
                temp_info_object = {}
                
                for j,element in enumerate(row):
                    #print "Row element------>"+element+"<------"
                    
                    temp_info_object[headers_list[j]] = str(element)

                temp_info_object["csv_file_path"] = 'input_docs/'+filename+'.csv'
                
                create_parliamentarian(**temp_info_object)

    @staticmethod
    def create_parliamentarians():
        for u in ((True,u"germany",u"12",u"Heike Baehrens",u"SPD",u"http://www.bundestag.de/abgeordnete18/biografien/B/baehrens_heike/259366",u"http://www.spdfraktion.de/abgeordnete/baehrens?wp=18",u"http://www.heike-baehrens.de/",u"http://www.bundestag.de/image/241886/3x4/235/314/48a840ebb22145194aa96b629dab8efb/da/baehrens_heike_gross.jpg",u"http://www.spdfraktion.de/system/files/styles/tablet_spdfraktion_1x_person_big/private/images/baehrens_hehike_615x720.jpg?itok=UUzI7-ih&timestamp=1458319448",u"Baden-Württemberg",u"Göppingen",u"263",u"Bankkauffrau, Dipl.-Religionspädagogin, Diakonie-Geschäftsführerin",u"Berlin",u"Platz der Republik 1",u"11011",u"Berlin",u"heike.baehrens@bundestag.de",u"030 / 227 74157",u"Göppingen",u"Schillerplatz 10",u"73033",u"Göppingen",u"info@heike-baehrens.de",u"07161 / 9883546",u"",u"21.09.1955 in Bevern (Niedersachsen)",u"",u"",u"",u"Arbeitsgruppe Gesundheit",u"Arbeitsgruppe Inklusion",u"18"),
                  (False,u"germany",u"14",u"Ulrike Bahr",u"SPD",u"http://www.bundestag.de/abgeordnete18/biografien/B/bahr_ulrike/258242",u"http://www.spdfraktion.de/abgeordnete/bahr-ulrike?wp=18","http://ulrike-bahr.de/",u"http://www.bundestag.de/image/240836/3x4/235/314/c3aac91e7c91d5e01134d9772d7a9b26/MW/bahr_ulrike_gross.jpg",u"http://www.spdfraktion.de/system/files/styles/tablet_spdfraktion_1x_person_big/private/images/bahr_ulrike_615x720.jpg?itok=cWk1PTa7&timestamp=1458319448",u"Bayern",u"Augsburg-Stadt",u"252",u"Lehrerin",u"Berlin",u"Platz der Republik 1",u"11011",u"Berlin",u"ulrike.bahr@bundestag.de",u"030 / 227 77282",u"Augsburg",u"Schaezlerstraße 13",u"86150",u"Augsburg",u"ulrike.bahr.wk@bundestag.de",u"0821 / 6505440",u"",u"25.04.1964 in Nördlingen",u"",u"",u"",u"Arbeitsgruppe Demografischer Wandel",u"Arbeitsgruppe Demokratie",u"18")
                  ):
            parlamientarian = create_parliamentarian(
                active=u[0],
                country=u[1],
                member_id=u[2],
                full_name=u[3],
                party=u[4],
                parliament_member_url=u[5],
                party_member_url=u[6],
                personal_url=u[7],
                photo_main_url=u[8],
                photo_backup_url=u[9],
                constituency_main=u[10],
                constituency_detail=u[11],
                constituency_number=u[12],
                profession=u[13],
                office_constituency_main=u[14],
                office_address_main=u[15],
                office_postal_code_main=u[16],
                office_location_main=u[17],
                office_email_main=u[18],
                office_phonenumber_main=u[19],
                office_constituency_backup=u[20],
                office_address_backup=u[21],
                office_postal_code_backup=u[22],
                office_location_backup=u[23],
                office_email_backup=u[24],
                office_phonenumber_backup=u[25],
                ministerial_role_information=u[26],
                birth_details=u[27],
                confession=u[28],
                family_status=u[29],
                parliament_join_date=u[30],
                extra_role_information=u[31],
                extra_role_information2=u[32],
                legislation_number=u[33]
                
            )
            

class CreateParliamentarianCommand(Command):
    """Create a role"""

    option_list = (
        Option('-a', '--active', dest='active', default=''),
        Option('-c', '--country', dest='country', default=''),
        Option('-mid', '--member_id', dest='member_id', default=''),
        Option('-n', '--full_name', dest='full_name', default=''),
        Option('-p', '--party', dest='party', default=''),
        Option('-pamu', '--parliament_member_url', dest='parliament_member_url', default=''),
        Option('-pmu', '--party_member_url', dest='party_member_url', default=''),
        Option('-peu', '--personal_url', dest='personal_url', default=''),
        Option('-ph', '--photo_main_url', dest='photo_main_url', default=''),
        Option('-cm', '--constituency_main', dest='constituency_main', default=''),
        Option('-cd', '--constituency_detail', dest='constituency_detail', default=''),
        Option('-ccm', '--office_constituency_main', dest='office_constituency_main', default=''),
        Option('-acm', '--office_address_main', dest='office_address_main', default=''),
        Option('-plz', '--office_postal_code_main', dest='office_postal_code_main', default=''),
        Option('-loc', '--office_location_main', dest='office_location_main', default=''),
        Option('-em', '--office_email_main', dest='office_email_main', default=''),
        Option('-tel', '--office_phonenumber_main', dest='office_phonenumber_main', default=''),
        
    )

    @commit
    def run(self, **kwargs):
        
        create_parliamentarian(**kwargs)
        print('Parliamentarian "%(full_name)s" created successfully.' % kwargs)
        
        
def create_parliamentarian(**kwargs):
    """Creates and returns a new parliamentarian from the given parameters."""

    parliamentarian = Parliamentarian(**kwargs)
    parliamentarian.save()
    

class PopulateCampaignDB(Command):
    """Create a role"""

    option_list = (
        Option('-c', '--country', dest='country', default='germany'),
        
    )

    @commit
    def run(self, **kwargs):
            
        usernames_list = ["user","matt","joe","jill","tiya"]
        participant_list = []
        
        for username in usernames_list:
            user = User.objects(username=username)[0]
            participant = create_participant(referenced_user = user)
            
            participant_list.append(participant)
                
        creator_campaign = participant_list[0]
                
        talking_point = create_talking_point(talking_point = "Ask his favourite colour")
        feedback = create_feedback(question = "What's your favourite colour?",feedback="Blue...NO! RED!")
        country = create_country(country="germany",country_code="DE")
        parliamentarian = Parliamentarian.objects(member_id="1")[0]
        call = create_call(name_responder="Adolfo",feedback_fields=[feedback],parliamentarian = parliamentarian, participant = creator_campaign, call_completed = True)
        campaign = create_campaign(country_list=[country],
                                   participant_list = participant_list,
                                   call_list = [call], 
                                   creator_campaign = creator_campaign, 
                                   parliamentarian_list = [parliamentarian],
                                   talking_point_list = [talking_point],
                                   title = "Save the Bees",
                                   description = "This is a description of the campaign.Here you can learn about the specific action we're campaigning for",
                                   campaign_code = "3oZsl0Ls7Qz7SO0F5OTb",
                                   specific_guidelines = "If the Devil responds and wants to make a deal, hang up.",
                                   campaign_picture = 'static/img/campaign_pictures/savebees.jpg' )
        
        talking_point = create_talking_point(talking_point = "Threaten their pets with chocolate")
        feedback = create_feedback(question = "What's your pets name?",feedback="Elefanto Magento")
        parliamentarian = Parliamentarian.objects(member_id="2")[0]
        call = create_call(name_responder="Elon Musk",feedback_fields=[feedback],parliamentarian = parliamentarian, participant = participant_list[3], call_completed = True)
        campaign = create_campaign(country_list=[country],
                                   participant_list = [participant_list[2],participant_list[3]],
                                   call_list = [call], 
                                   creator_campaign = creator_campaign, 
                                   parliamentarian_list = [parliamentarian],
                                   talking_point_list = [talking_point],
                                   title = "Handicapped access to public transit bill",
                                   description = "This is a description of the campaign.Here you can learn about the specific action we're campaigning for",
                                   campaign_code = "G3rxrJOSQf7AsEGlmV6F",
                                   specific_guidelines = "Try to leave awkward pauses in between sentences. Makes them tense.",
                                   campaign_picture = 'static/img/campaign_pictures/disabilities.png' )
        
        talking_point = create_talking_point(talking_point = "Threaten their pets with chocolate")
        feedback = create_feedback(question = "What's your pets name?",feedback="Elefanto Magento")
        parliamentarian = Parliamentarian.objects(member_id="2")[0]
        call = create_call(name_responder="Elon Musk",feedback_fields=[feedback],parliamentarian = parliamentarian, participant = participant_list[2], call_completed = True)
        campaign = create_campaign(country_list=[country],
                                   participant_list = [participant_list[0],participant_list[3],participant_list[2],participant_list[1]],
                                   call_list = [call], 
                                   creator_campaign = creator_campaign, 
                                   parliamentarian_list = [parliamentarian],
                                   talking_point_list = [talking_point],
                                   title = "Handicapped2",
                                   description = "This is a description of the campaign.Here you can learn about the specific action we're campaigning for",
                                   campaign_code = "Lvg3dHEfvMztHxweLaGx",
                                   specific_guidelines = "Try to leave awkward pauses in between sentences. Makes them tense.",
                                   campaign_picture = 'static/img/campaign_pictures/disabilities.png' )
        
        talking_point = create_talking_point(talking_point = "Threaten their pets with chocolate")
        feedback = create_feedback(question = "What's your pets name?",feedback="Elefanto Magento")
        parliamentarian = Parliamentarian.objects(member_id="2")[0]
        call = create_call(name_responder="Elon Musk",feedback_fields=[feedback],parliamentarian = parliamentarian, participant = participant_list[1], call_completed = True)
        campaign = create_campaign(country_list=[country],
                                   participant_list = [participant_list[2],participant_list[4],participant_list[1]],
                                   call_list = [call], 
                                   creator_campaign = creator_campaign, 
                                   parliamentarian_list = [parliamentarian],
                                   talking_point_list = [talking_point],
                                   title = "Handicapped3",
                                   description = "This is a description of the campaign.Here you can learn about the specific action we're campaigning for",
                                   campaign_code = "Xz39xcZUcdSsUFXHunAF",
                                   specific_guidelines = "Try to leave awkward pauses in between sentences. Makes them tense.",
                                   campaign_picture = 'static/img/campaign_pictures/disabilities.png' )
        
        talking_point = create_talking_point(talking_point = "Threaten their pets with chocolate")
        feedback = create_feedback(question = "What's your pets name?",feedback="Elefanto Magento")
        parliamentarian = Parliamentarian.objects(member_id="2")[0]
        call = create_call(name_responder="Elon Musk",feedback_fields=[feedback],parliamentarian = parliamentarian, participant = participant_list[0], call_completed = True)
        campaign = create_campaign(country_list=[country],
                                   participant_list = [participant_list[0],participant_list[4],participant_list[3]],
                                   call_list = [call], 
                                   creator_campaign = creator_campaign, 
                                   parliamentarian_list = [parliamentarian],
                                   talking_point_list = [talking_point],
                                   title = "Handicapped4",
                                   description = "This is a description of the campaign.Here you can learn about the specific action we're campaigning for",
                                   campaign_code = "EmEvzSLMNtUxyBN5kPBL",
                                   specific_guidelines = "Try to leave awkward pauses in between sentences. Makes them tense.",
                                   campaign_picture = 'static/img/campaign_pictures/disabilities.png' )
        
        talking_point = create_talking_point(talking_point = "Threaten their pets with chocolate")
        feedback = create_feedback(question = "What's your pets name?",feedback="Elefanto Magento")
        parliamentarian = Parliamentarian.objects(member_id="2")[0]
        call = create_call(name_responder="Elon Musk",feedback_fields=[feedback],parliamentarian = parliamentarian, participant = participant_list[3], call_completed = True)
        campaign = create_campaign(country_list=[country],
                                   participant_list = [participant_list[2],participant_list[3]],
                                   call_list = [call], 
                                   creator_campaign = creator_campaign, 
                                   parliamentarian_list = [parliamentarian],
                                   talking_point_list = [talking_point],
                                   title = "Handicapped5",
                                   description = "This is a description of the campaign.Here you can learn about the specific action we're campaigning for",
                                   campaign_code = "jqp9cGNwzLGUEzS8texY",
                                   specific_guidelines = "Try to leave awkward pauses in between sentences. Makes them tense.",
                                   campaign_picture = 'static/img/campaign_pictures/disabilities.png' )
        
        talking_point = create_talking_point(talking_point = "Threaten their pets with chocolate")
        feedback = create_feedback(question = "What's your pets name?",feedback="Elefanto Magento")
        parliamentarian = Parliamentarian.objects(member_id="2")[0]
        call = create_call(name_responder="Elon Musk",feedback_fields=[feedback],parliamentarian = parliamentarian, participant = participant_list[0], call_completed = True)
        campaign = create_campaign(country_list=[country],
                                   participant_list = [participant_list[0]],
                                   call_list = [call], 
                                   creator_campaign = creator_campaign, 
                                   parliamentarian_list = [parliamentarian],
                                   talking_point_list = [talking_point],
                                   title = "Handicapped6",
                                   description = "This is a description of the campaign.Here you can learn about the specific action we're campaigning for",
                                   campaign_code = "2KLAUajkGZjp5wNbRS4V",
                                   specific_guidelines = "Try to leave awkward pauses in between sentences. Makes them tense.",
                                   campaign_picture = 'static/img/campaign_pictures/disabilities.png' )
    
        print('Run population successfully.' % kwargs)
        

class UnpopulateCampaignDB(Command):
    
    """Drops all tables and recreates them"""
    def run(self, **kwargs):
        self.drop_collections()

    @staticmethod
    def drop_collections():
        Participant.drop_collection()
        TalkingPoint.drop_collection()
        Feedback.drop_collection()
        Country.drop_collection()
        Call.drop_collection()
        Campaign.drop_collection()

def create_participant(**kwargs):
    participant = Participant(**kwargs)
    participant.save() 
    return participant 
    
    
def create_talking_point(**kwargs):
    talking_point = TalkingPoint(**kwargs)
    talking_point.save()
    return talking_point
    

def create_feedback(**kwargs):
    feedback = Feedback(**kwargs)
    feedback.save()
    return feedback
    
    
def create_country(**kwargs):
    country = Country(**kwargs)
    country.save()
    return country
    
            
def create_call(**kwargs):
    call = Call(**kwargs)
    call.save()
    return call
            
def create_campaign(**kwargs):
                
    campaign = Campaign(**kwargs)
    campaign.save()

    
    

    
  
from flask_application.models import db, FlaskDocument
from flask_application.users.models import Role, User

import datetime

class Parliamentarian(FlaskDocument):
    
    active = db.BooleanField(default=True)
    last_updated = db.DateTimeField(default=datetime.datetime.now)
    csv_file_path = db.StringField(max_length=255)
    country = db.StringField(max_length=63)
    member_id = db.StringField(max_length=31)
        
    full_name = db.StringField(max_length=63)
    party = db.StringField(max_length=63)
    
    parliament_member_url = db.StringField(max_length=255)
    party_member_url = db.StringField(max_length=255)
    personal_url = db.StringField(max_length=255)
    
    photo_main_url = db.StringField(max_length=255)
    photo_backup_url = db.StringField(max_length=255)
    
    constituency_main = db.StringField(max_length=255)
    constituency_detail = db.StringField(max_length=255)
    constituency_number = db.StringField(max_length=255)
    
    profession = db.StringField(max_length=255)
    
    office_constituency_main = db.StringField(max_length=255)
    office_address_main = db.StringField(max_length=255)
    office_postal_code_main = db.StringField(max_length=255)
    office_location_main = db.StringField(max_length=255)
    office_email_main = db.StringField(max_length=255)
    office_phonenumber_main = db.StringField(max_length=255)
    
    office_constituency_backup = db.StringField(max_length=255)
    office_address_backup = db.StringField(max_length=255)
    office_postal_code_backup = db.StringField(max_length=255)
    office_location_backup = db.StringField(max_length=255)
    office_email_backup = db.StringField(max_length=255)
    office_phonenumber_backup = db.StringField(max_length=255)
    
    ministerial_role_information = db.StringField(max_length=255)
    birth_details = db.StringField(max_length=255)
    confession = db.StringField(max_length=255)
    family_status = db.StringField(max_length=255)
    
    parliament_join_date = db.StringField(max_length=255)
    
    extra_role_information = db.StringField(max_length=255)
    extra_role_information2 = db.StringField(max_length=255)
    
    legislation_number = db.StringField(max_length=255)
    
    calling_score = db.IntField(default=0)
    
    #calls_list = db.ListField(db.ReferenceField(Call),default=[])
    #To obtain calling_score and calls_list, just query for this parliamentarian in the list of all Call objects instead
     
class Participant(FlaskDocument):

    referenced_user = db.ReferenceField(User)
    
    #calls_list = db.ListField(db.ReferenceField(Call),default=[])
    #To obtain calls_list, just query for this Participant in the list of all Call objects instead
    
class TalkingPoint(FlaskDocument):    

    talking_point = db.StringField(max_length=255)
    
class Feedback(FlaskDocument):
    
    question = db.StringField(max_length=255)
    feedback = db.StringField(max_length=1023)
    
class Country(FlaskDocument):
    
    country = db.StringField(max_length=63)
    country_code = db.StringField(max_length=63)

class Call(FlaskDocument):

    call_completed = db.BooleanField(default=False)

    call_starts = db.DateTimeField()
    call_ends = db.DateTimeField(default=datetime.datetime.now)

    name_responder = db.StringField(max_length=255)
    score = db.IntField(default=0)
    feedback_fields = db.ListField(db.ReferenceField(Feedback),default=[])
    
    parliamentarian = db.ReferenceField(Parliamentarian)
    participant = db.ReferenceField(Participant)
    
class Campaign(FlaskDocument):
    
    draft = db.BooleanField(default=True)
    completion = db.IntField(default=0) #When going through the steps of Campaign Creation,
                                        # 0 = start, 1 = step1 (details), 2 = step2 (parliamentarians), 3 =  step3 (guidelines) , 4 = step4 (preview) , 5 = step5 (done)
    
    active = db.BooleanField(default=False)
    last_updated = db.DateTimeField(default=datetime.datetime.now)
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    
    campaign_picture = db.StringField(max_length=127)
    
    campaign_code = db.StringField(max_length=1023)
    country_list = db.ListField(db.ReferenceField(Country),default=[])
    
    participant_list = db.ListField(db.ReferenceField(Participant),default=[])
    call_list = db.ListField(db.ReferenceField(Call),default=[])
    
    creator_campaign = db.ReferenceField(Participant)
    parliamentarian_list = db.ListField(db.ReferenceField(Parliamentarian),default=[])
    
    title = db.StringField(max_length=127)
    description = db.StringField(max_length=1023)
    specific_guidelines = db.StringField(max_length=1023)
    talking_point_list = db.ListField(db.ReferenceField(TalkingPoint),default=[])
    
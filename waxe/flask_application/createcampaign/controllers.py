from flask import Blueprint

from flask.ext.security import login_required

from flask_application.controllers import TemplateView

createcampaign = Blueprint('createcampaign', __name__)


class CreateCampaignStep1(TemplateView):
    blueprint = createcampaign
    route = '/createcampaign/step1'
    route_name = 'createcampaign_step1'
    template_name = 'createcampaign/step1.html'
    decorators = []

    def get_context_data(self, *args, **kwargs):
        return {
            'content': 'This is the create campaign step 1 page'
        }
        
class CreateCampaignStep2(TemplateView):
    blueprint = createcampaign
    route = '/createcampaign/step2'
    route_name = 'createcampaign_step2'
    template_name = 'createcampaign/step2.html'
    decorators = []

    def get_context_data(self, *args, **kwargs):
        return {
            'content': 'This is the create campaign step 2 page, says me'
        }
        
class CreateCampaignStep3(TemplateView):
    blueprint = createcampaign
    route = '/createcampaign/step3'
    route_name = 'createcampaign_step3'
    template_name = 'createcampaign/step3.html'
    decorators = []

    def get_context_data(self, *args, **kwargs):
        return {
            'content': 'This is the create campaign step 3 page'
        }
        
class CreateCampaignStep4(TemplateView):
    blueprint = createcampaign
    route = '/createcampaign/step4'
    route_name = 'createcampaign_step4'
    template_name = 'createcampaign/step4.html'
    decorators = []

    def get_context_data(self, *args, **kwargs):
        return {
            'content': 'This is the create campaign step 4 page'
        }
        
class CreateCampaignStep5(TemplateView):
    blueprint = createcampaign
    route = '/createcampaign/step5'
    route_name = 'createcampaign_step5'
    template_name = 'createcampaign/step5.html'
    decorators = []

    def get_context_data(self, *args, **kwargs):
        return {
            'content': 'This is the create campaign step 5 page'
        }

 

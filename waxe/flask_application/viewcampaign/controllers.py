from flask import Blueprint

from flask.ext.security import login_required

from flask_application.controllers import TemplateView

from flask_application.createcampaign.models import *

viewcampaign = Blueprint('viewcampaign', __name__)


class ViewCampaignDescription(TemplateView):
    blueprint = viewcampaign
    route = '/viewcampaign/description/<campaign_code>'
    route_name = 'viewcampaign_description'
    template_name = 'viewcampaign/description.html'
    decorators = []

    def get_context_data(self, *args, **kwargs):
        
        campaign = Campaign.objects(campaign_code=kwargs['campaign_code'])[0]
        
        user = User.objects(username="user")[0]
        participant = Participant.objects(referenced_user = user)[0]
        
        parliamentarian_list = campaign.parliamentarian_list
        call_list = campaign.call_list
        
        total_parliamentarians = len(parliamentarian_list)
        parliamentarians_contacted_list = []
        
                
        for call in call_list:
            for parliamentarian in parliamentarian_list:
                if Call.objects(id = call.id,parliamentarian = parliamentarian, participant = participant).count() > 0:
                    if parliamentarian not in parliamentarians_contacted_list:
                        parliamentarians_contacted_list.append(parliamentarian)
                
              
        total_parliamentarians_contacted = len(parliamentarians_contacted_list)
        
        return {
            'content': 'This is the view campaign description page',
            'campaign': campaign,
            'parliamentarian_list': parliamentarian_list,
            'call_list': call_list,
            'parliamentarians_contacted_list': parliamentarians_contacted_list,
            'participant_list': campaign.participant_list,
            'total_parliamentarians': total_parliamentarians,
            'total_parliamentarians_contacted': total_parliamentarians_contacted,
            'talking_point_list': campaign.talking_point_list,
            'participant_list': campaign.participant_list
        }
        
class ViewCampaignAction(TemplateView):
    blueprint = viewcampaign
    route = '/viewcampaign/action'
    route_name = 'viewcampaign_action'
    template_name = 'viewcampaign/action.html'
    decorators = []

    def get_context_data(self, *args, **kwargs):
        return {
            'content': 'This is the view campaign action page'
        }

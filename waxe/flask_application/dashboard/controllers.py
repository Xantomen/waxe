from flask import Blueprint

from flask.ext.security import login_required

from flask import Flask, render_template, request, redirect, url_for

from flask_application.controllers import TemplateView

from flask_application.createcampaign.models import *

dashboard = Blueprint('dashboard', __name__)

class DashboardView(TemplateView):
    blueprint = dashboard
    route = '/dashboard'
    route_name = 'dashboard'
    template_name = 'dashboard/dashboard.html'
    decorators = []
    
    def get(self):
        
        user = User.objects(username="user")[0]
        participant = Participant.objects(referenced_user = user)[0]
        campaign_list = Campaign.objects(creator_campaign = participant)
        
        campaign = None
        
        if len(campaign_list) > 0:
            return redirect(self.route_name+"/"+campaign_list[0].campaign_code)
        else:
            data = {
                'content': 'This is the dashboard page',
                'campaign_list': campaign_list,
                'campaign': campaign
            }
                            
            return render_template(self.template_name, **data)
        

class DashboardViewCampaignCode(TemplateView):
    blueprint = dashboard
    route = '/dashboard/<campaign_code>'
    route_name = 'dashboard_campaigncode'
    template_name = 'dashboard/dashboard.html'
    decorators = []
    
    def get(self, campaign_code):
                    
        user = User.objects(username="user")[0]
        participant = Participant.objects(referenced_user = user)[0]
        campaign_list_temp = Campaign.objects(creator_campaign = participant).only('title','campaign_picture','creation_date','participant_list','call_list','campaign_code','parliamentarian_list')
        
        campaign_info_list = []
        
        for campaign in campaign_list_temp:
            
            campaign_temp = campaign
            campaign_temp.participant_count = len(campaign.participant_list)
            campaign_temp.call_count = len(campaign.call_list)
            
            del campaign_temp.participant_list
            del campaign_temp.call_list
            
            campaign_info_list.append(campaign_temp)
                        
        try:
            campaign = Campaign.objects(campaign_code = campaign_code)[0]
            
            print campaign.title
            
            participant_info_list = []
            
            for participant in campaign.participant_list:
               
                info = {}
                
                referenced_user = participant.referenced_user
              
                info['first_name'] = referenced_user.first_name
                info['last_name'] = referenced_user.last_name
                info['email'] = referenced_user.email
                
                calls_made = Call.objects(participant = participant,call_completed = True)
                                 
                calls_made_this_campaign = Campaign.objects(call_list__in = calls_made, campaign_code = campaign_code)
                                                                                
                info['calls_made'] = calls_made_this_campaign.count()
              
                participant_info_list.append(info)
                
        except:
            campaign = None
            participant_info_list = []
                
        data = {
            'content': 'This is the dashboard page',
            'campaign_info_list': campaign_info_list,
            'participant_info_list': participant_info_list,
            'campaign_code': campaign_code,
            'parliamentarian_list_count': len(campaign.parliamentarian_list)
        }
        
                          
        return render_template(self.template_name, **data)
             